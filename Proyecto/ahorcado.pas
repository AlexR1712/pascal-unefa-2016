program Ahorcado;
uses crt;
const
	max = 10;
	(* Palabras segun nivel (1,2,3) en array de forma constante. *)
	(* Tres letras *)
	PalabrasN1: array[1..max] of string =(
     'Muy','Uno','Osa','Red',
     'Uso','Ver','Dos','Aji',
     'Alo','Era');
	(* Cinco letras *)
	PalabrasN2: array[1..max] of string =(
     'Andar','Anaco','Acero','Adobo',
     'Ajeno','Algun','Alzar','Elite',
     'Apagar','Apoyo');
	(* Siete letras *)
	PalabrasN3: array[1..max] of string =(
     'Cajeros','Cachito','Cajitas','Cajones',
     'Bailado','Bajista','Balones','Ganador',
     'Exitoso','Grafico');
  (*Pistas*)
  PistaN1: array[1..max] of string =(
     'Se utiliza para intensificar una cualidad',
     'Se refiere a una unidad',
     'Es un animal que le gusta la miel',
     'Se usa para pescar pero tambien es un termino informatico',
     'Acción de usar',
     'Es un sentido que usamos con los ojos',
     'Equivale a decir Uno sobre Dos por Cuatro',
     'Planta que en tambien se denomina Chile',
     'Ring Ring Ring...',
     'Una linea de tiempo o periodo'
     );

  (* Cinco letras *)
  PistaN2: array[1..max] of string =(
     'Ir de un lugar a otro dando pasos',
     'Ciudad del Estado Anzoategui Venezuela',
     'Mezcla de hierro con una cantidad de carbono determinado',
     'Condimento o salsa a base de diferentes chiles y otros elementos para realzar el sabor de la comida.',
     'Que pertenece o corresponde a otro',
     'Forma de "alguno" usado delante de nombres masculinos singulares',
     'Poner una cosa o a una persona en una posición alta o más alta de la que tenía.',
     'Grupo minoritario de personas que tienen un estatus superior al resto de las personas de la sociedad.',
     'Es lo contrario de Encender',
     'Cosa que sostiene algo o que sirve para sostener.'
     );
  (* Siete letras *)
  PistaN3: array[1..max] of string =(
     'Maquinas desde donde se saca dinero con tarjetas',
     'Es un pan, pero tiene cachos como un toro',
     'Generalmente son de Carton y se usa para almacenar cosas pequenas',
     'Son de madera por lo general y alli guardamos cosas.',
     'Participio pasado de Bailar',
     'Persona que toca el Bajo',
     'Se dice que si pasan a las arquerias es Gol que son?',
     'Lo inverso de Perdedor',
     'Cuando compila la primera vez te sientes?',
     'Se usa para representar datos en forma de imagenes'
     );

	MaxIntentos = 8;

type 
	 letrasArray = array[1..max] of string;
	 entero = integer;
	 letra = string;
label 
   salir;
var
	palabraNivel: letrasArray;
	palabra: letra;
  opcion,letrap:letra;


(* Funcion para determinar la cantidad de caracteres de una palabra *)
function cantLetras(y:string): Integer;
begin
	cantLetras:= Length(y);
end;


{*** ####Procedures#### ****}
procedure Menu(var opcion:string);
begin
  writeln('Bienvenido por favor seleccione una Opcion: [1] Jugar  [0] para Salir');
  repeat
    begin
      readln(opcion);
      case opcion of
        '1': begin
              writeln('Opcion 1 seleccionado');
             end;
        '0': begin
              Exit;
             end;
        else 
          Writeln('No ha elegido una opcion correcta, por favor intente de nuevo.');
      end;

    end;
  until ((opcion = '1') or (opcion = '0'));
end;


procedure Nivel(var palabra:letra; opcion:string);
	var nivel: string; numeroPalabra: integer;

begin
  clrscr;
	writeln('Seleccione el Nivel que desea Jugar');
	writeln('[1] Nivel Facil');
	writeln('[2] Nivel Intermedio');
	writeln('[3] Nivel Dificil');
  randomize;
  numeroPalabra:= random(max);
	repeat
	 begin
    readln(nivel);
	case nivel of
    '1' : begin
         // numeroPalabra:= azar(PalabrasN1);
         

         palabra:= upcase(PalabrasN1[numeroPalabra]);
          writeln('Ha selecionado el nivel 1 [Facil] (3 Letras) y la palabra es ', palabra);
          end;
    '2' : begin
        //  numeroPalabra:= azar(PalabrasN2);
          palabra:= upcase(PalabrasN2[numeroPalabra]);
          writeln('Ha selecionado el nivel 2 [Intermedio] (5 Letras) y la palabra es ', palabra);          
          end;
    '3' : begin
         // numeroPalabra:= azar(PalabrasN3);
          palabra:= upcase(PalabrasN3[numeroPalabra]);
          writeln('Ha selecionado el nivel 3 [Dificil] (7 Letras) y la palabra es ', palabra);          
          end;
    else Writeln('No ha elegido una opcion correcta, por favor intente de nuevo.');
    end;
  end;
    until ((nivel = '1') or (nivel = '2') or (nivel = '3'));
end;


procedure Jugar(var palabra:letra; letrap:letra);
  var i,j,x,y,noencontrado,fallos:entero; nuevapalabra:letra;
begin
  x:= 10;
  y:= 20;
  noencontrado:= 0;
  fallos:= 0;
  nuevapalabra:='';
  (*Escribir la cantidad de espacios*)
for i := 1 to Length(palabra) do
  begin
    gotoxy(x,y);
    textcolor(green);
    write('_ ');
  end;

repeat
  begin
  gotoxy(8,7);
  writeln('Escriba una letra de la palabra a adivinar');
  readln(letrap);
  letrap:= upcase(letrap);  
  
  for j := 1 to Length(palabra) do
    begin
    
     if (letrap = palabra[j]) then
     begin
{If interno}if (i=1) then
         begin
           gotoxy(x,y);
           write(letrap);      
         end
{Else interno}else 
         begin
           gotoxy(x+i-1,y);
           write(letrap);  
         end;
      end
    else  
    begin 
    noencontrado:= noencontrado + 1;
      if (noencontrado = Length(palabra)) then
       begin
       fallos:= fallos + 1;
       gotoxy(10,8);
       writeln('Haz fallado te quedan ',MaxIntentos-fallos, 'intentos');
       end; 
    end;
    end; {For j}
  end; {Repeat}
until ((fallos = MaxIntentos) or (Length(nuevapalabra) = Length(palabra)));
end; {Procedure}



// * Cuerpo Principal

begin
    Menu(opcion);
    Nivel(palabra,opcion);
    Jugar(palabra,letrap);
    readln;
end.
