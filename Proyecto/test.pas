(* ##Funciones Basicas## *)

(*Funcion para obtener un numero aleatorio segun la cantidad de item de un arreglo*)
function azar(x:letrasArray): String;
var cant:integer;
begin
  cant:= High(x);
  randomize;
  azar := random(cant);
end;



procedure Jugar();
begin
writeln('El juego a comenzado.');
end;



procedure Fondo;
{Procedure encargado de crear un marco en el fondo del programa}
var fila,columna:byte;
begin
  for fila:=2 to 24 do begin
    if (fila = 2) or (fila = 24) then 
      begin
        for columna:=2 to 79 do begin
        textbackground(blue);
        gotoxy(columna,fila);
        write(' ');
      end;
 end  else  begin
      textbackground(blue);
      gotoxy(2,fila);
      write(' ');
      gotoxy(79,fila);
      write(' ');
             end;
    end;
end;
{Procedures cuando el Usuario Falla }
 procedure PrimerFallo;       { Primer fallo: }
 var
   j: byte;                   { Dibujamos la "plataforma" }
 begin
   for j := 50 to 60 do
     begin
     gotoxy(j,20);
     write('-');
     end;
 end;
 
 
 procedure SegundoFallo;       { Segundo fallo: }
 var
   j: byte;                   { Dibujamos el "palo vertical" }
 begin
   for j := 14 to 19 do
     begin
     gotoxy(53,j);
     write('|');
     end;
 end;
 
 
 procedure TercerFallo;       { Tercer fallo: }
 var
   j: byte;                   { Dibujamos el "palo superior" }
 begin
   for j := 53 to 57 do
     begin
     gotoxy(j,14);
     write('-');
     end;
 end;
 
 procedure CuartoFallo;       { Cuarto fallo: }
 var
   j: byte;                   { Dibujamos la "plataforma" }
 begin
   gotoxy(57,15);
   write('|');
 end;
 
 procedure QuintoFallo;       { Quinto fallo: }
 var
   j: byte;                   { Dibujamos la "plataforma" }
begin
   gotoxy(56,16);
   write(' O');
   gotoxy(56,17);
   write('/|\');
   gotoxy(56,18);
   write('/ \');
 
   for j := 50 to 60 do
     begin
     gotoxy(j,20);
     write('-');
     end;
 end;
